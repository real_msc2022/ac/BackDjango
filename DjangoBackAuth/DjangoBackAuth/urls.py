
from django.contrib import admin
from django.urls import path,include
from AuthUser.views import Home

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('AuthUser.urls')),
     path("accounts/", include("allauth.urls")),
     path("github", Home.as_view(), name="home"),
]
